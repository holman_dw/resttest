package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"

	"github.com/pelletier/go-toml"
)

func main() {
	var rule string
	flag.StringVar(&rule, "rule", "", "`NAME` of the rule to be run")
	flag.StringVar(&rule, "r", "", "`NAME` of the rule to be run (abbrv)")
	flag.Usage = func() {
		n := os.Args[0]
		errorf("usage of %s:\n", n)
		errorf("%s [OPTIONS] config.toml\n", n)
		flag.PrintDefaults()
	}
	flag.Parse()
	if flag.NArg() < 1 {
		errorf("expected configuration toml file as a positional argument")
		return
	}
	abs, err := filepath.Abs(flag.Arg(0))
	if err != nil {
		errorf("error with the filepathof %s: %v", flag.Arg(0), err)
		return
	}
	config, err := parseToml(abs)
	if err != nil {
		errorf("error parsing config: %v", err)
		return
	}
	found := false
	for _, r := range config.Request {
		if rule != "" && rule != r.Name {
			continue
		}
		found = true
		resp, err := r.Run()
		if err != nil {
			errorf("error running rule %s: %v", r.Name, err)
			return
		}
		code := resp.StatusCode
		text := http.StatusText(code)
		fmt.Printf("%s: %d %s\n%s\n", r.Name, code, text, string(resp.JSON))
	}
	if !found {
		errorf("rule %s not found or no rule supplied", rule)
		return
	}
}

// Response from an API
type Response struct {
	JSON       []byte
	StatusCode int
}

// Config is the application configuration read from a toml file
type Config struct {
	Request []Request
}

// Request is the data needed to make an JSON HTTP request
// against a RESTful service
type Request struct {
	Name        string
	URL         string
	Method      string
	JSONFile    string
	JSONPayload string
}

func (r Request) validate() error {
	hasJSONFile := r.JSONFile != ""
	hasJSONPayload := r.JSONPayload != ""
	if !hasJSONFile && !hasJSONPayload {
		return fmt.Errorf("json file or payload must be present")
	}
	if hasJSONFile && hasJSONPayload {
		return fmt.Errorf("only one of jsonfile or jsonpayload can be provided")
	}
	if r.Method != http.MethodPost {
		return fmt.Errorf(r.Method + " is not yet implented. only POST requests are implemented")
	}
	return nil
}

// Run executes a Request
func (r Request) Run() (Response, error) {
	resp := Response{}
	if err := r.validate(); err != nil {
		return resp, err
	}
	var j []byte
	var err error
	switch r.JSONFile != "" {
	case true:
		j, err = readJSONFile(r.JSONFile)
		if err != nil {
			return resp, err
		}
	case false:
		j = []byte(r.JSONPayload)
		if !json.Valid(j) {
			return resp, fmt.Errorf("invalid json in " + string(j))
		}
	}
	status, b, err := postJSON(r.URL, j)
	if err != nil {
		return resp, err
	}
	resp.JSON = b
	resp.StatusCode = status
	return resp, nil
}

func parseToml(path string) (Config, error) {
	b, err := readFile(path)
	if err != nil {
		return Config{}, err
	}
	config := Config{}
	if err := toml.Unmarshal(b, &config); err != nil {
		return Config{}, err
	}
	return config, nil
}

func readJSONFile(path string) ([]byte, error) {
	data, err := readFile(path)
	if err != nil {
		return []byte{}, err
	}
	if !json.Valid(data) {
		return []byte{}, fmt.Errorf("invalid json in %s", path)
	}
	return data, nil
}

func postJSON(url string, jsonData []byte) (int, []byte, error) {
	buf := bytes.NewBuffer(jsonData)
	resp, err := http.Post(url, "application/json", buf)
	if err != nil {
		return 0, []byte{}, err
	}
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, []byte{}, err
	}
	dst := bytes.Buffer{}
	err = json.Indent(&dst, b, "", "  ")
	return resp.StatusCode, dst.Bytes(), err
}

func readFile(path string) ([]byte, error) {
	b := []byte{}
	f, err := os.Open(path)
	if err != nil {
		return b, err
	}
	defer f.Close()
	stat, err := f.Stat()
	if err != nil {
		return b, err
	}
	b = make([]byte, stat.Size())
	_, err = f.Read(b)
	return b, err
}

func errorf(s string, args ...interface{}) (int, error) {
	return fmt.Fprintf(os.Stderr, s, args...)
}
