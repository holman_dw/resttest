# RestTest

`resttest` is a configurable command line RESTful API tester. Configuration
files (toml) and JSON files are used to make requests to APIs. At this time,
only `POST` requests with `JSON` payloads are supported. The response is
expected to be `JSON` as well. The toml configuration file is expected to
be a table with fields `name`, `url`, `method`, and one of `jsonpayload` or
`jsonfile`.

```
[[request]]
    name = "local"
    url = "http://localhost:8080/api/foo/bar"
    method = "POST"
    jsonpayload = """
    {
        "foo": {
            "bar": "baz"
        }
    }
    """
```
